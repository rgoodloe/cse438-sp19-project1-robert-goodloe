package com.example.project1.model

// holds information for food model
data class Food(var name: String,
                var calories: Int)