package com.example.project1.model

// holds information for exercise model
data class Exercise(var name: String,
                    var time: Int
)