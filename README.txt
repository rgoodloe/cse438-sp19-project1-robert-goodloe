* Is there anything that doesn't work? Why?
        Nope. Should be fully functional. The push to git may be a bit odd, but the source code is there.


* Is there anything that you did that you feel might be unclear? Explain it here.
        No, it is a simple calorie counter and exercise tracking app.

A description of the creative portion of the assignment
* Describe your feature
        For the creative portion of the app, I expanded the functionality to include an exercise tracker as well.
        I also included a feature in which the text auto-completes the exercise or food name, based on a hard-coded set of values in my strings.xml file.
        If a user begins to type any of these into the dialog to enter food or exercises, a suggestion will pop-up on the second letter.

* Why did you choose this feature?
        I chose the exercise feature because this seems to go hand-in-hand with  calorie counting in the fitness industry.

        I chose the auto complete text because of its convenience and the fact that most activities are repeated often. A jogger will mostly be logging jogging,
        and to have that pop upo quickly is nice.


* How did you implement it?
        While the exercise tracker does seem to be a simple rehashing of the original features of the app, it actually came with a lot of adjustments.
        After creating a duplicate fragment, adapter and layout, I realized that I needed to add the fragments to the back-stack in order to "pop" them off and return to
        the previous fragment. From there, it became apparent that once I hit back, all of the data stored in the list view in that fragment was destroyed. In order to fix this,
        I had to store my data in the MainActivity and instantiate local copies/update the fragment views upon inflating them. I learned a lot about the lifecycle of fragments and
        how to manipulate/pass data between them. I think however that if I were to do it again, I might have made each fragment an activity in itself, since the UI was
        not necessarily reused throughout the app.

        For the auto complete text view I created an array of strings in the strings.xml file. I put this array into an adapter that displayed the strings under the text view
        using androids built in simple_list_item_ 1 layout.
        
1. (10 / 10 Points) User can enter total calorie amount on start up
2. (8 / 8 Points) User can add new food item by name
3. (8 / 8 Points) User can add new food item by calorie
4. (4 / 4 Points) Adding new food items is done in a second activity
5. (5 / 5 Points) Calories remaining is updated with each new food item
6. (5 / 5 Points) Calorie consumed is updated with each new food item
7. (10 / 10 Points) The list of food items displays foods and their respective calories amounts
8. (10 / 10 Points) Color change when calorie count becomes negative
9. (10 / 10 Points) All inputs are filtered and error messages are displayed accordingly
10. (2 / 2 Points) Code is clean and commented
11. (2 / 3 Points) App is visually appealing
12. (15 / 15 Points) Creative portion - design your own feature(s)!

I like the exercise tracker, and I especially like how you have a whole separate part of the app for it instead of combining it with the calorie tracker.

You do have some extra UI stuff in the app (the drawer and the Settings button that doesn't do anything) that should be removed, which I took a point off for.

Total: 89 / 90
